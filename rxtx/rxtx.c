/*
 * main.c
 *
 * Created: 27-Jan-21 11:59:15 AM
 *  Author: Basilis Christianidis
 */ 

#include <xc.h>
#include <stdio.h>

#define F_CPU 8000000UL //the clock of the cpu (internal is 8 000 000 hz)
#define BRC ((F_CPU/16/9600)-1) //define the baud rate (usually 9600)

#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <util/delay.h>


#define bit_get(p,m) ((p) & (m)) //if(bit_get(foo, BIT(3)))
#define bit_set(p,m) ((p) |= (m)) //bit_set(foo, 0x01); bit_set(foo, BIT(5));
#define bit_clear(p,m) ((p) &= ~(m)) //bit_clear(foo, 0x40);
#define bit_flip(p,m) ((p) ^= (m)) //bit_flip(foo, BIT(0));
#define bit_write(c,p,m) (c ? bit_set(p,m) : bit_clear(p,m)) //bit_write(bit_get(foo, BIT(4)), bar, BIT(0)); (To set or clear a bit based on bit number 4:)
#define BIT(x) (0x01 << (x)) // 0<x<7
#define LONGBIT(x) ((unsigned long)0x00000001 << (x))


//TX functions
void sw(const char c[]); //print a string on serial
void swn(int num, int type, int ln); //print a Register or a number (no float), in hex(2) or in decimal(10)
void swf(double numf, uint8_t lnf, uint8_t extra_accur, uint8_t print_it); //Take a float and print it on serial

//RX functions
void readUntill(char c);

//======================TX START======================
volatile uint8_t len, k=0; //unsigned 8 bit integer
volatile char str[40], str_floatx[40];
//======================TX END=======================

//======================RX START=======================
char rxBuffer[128];
volatile char udr0 = '\0', rx_stop='\0'; /*  NULL = \0  */
volatile uint8_t rxReadPos = 0, rxWritePos = 0, readString =0;
//======================RX END=======================

//Functions
void init_rxtx_function(); //initializes only rxtx functionality


int main(void)
{
	
	init_rxtx_function();
    while(1)
    {
        //TODO:: Please write your application code 
    }
}



void init_rxtx_function()
{
	//==================================TX START====================================
	UBRR0H = (BRC >> 8); //Put BRC to UBRR0H and move it right 8 bits.
	UBRR0L = BRC;
	UCSR0B = (1 << TXEN0); //Trans enable
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); //8 BIT data frame

	//ENABLE interrupts
	sei();
	//==================================TX END=====================================
	//================================== RX START===========================================
	UCSR0B |= (1 << RXEN0) | (1 << RXCIE0); //RX enable and Receive complete interrupt enable
	//================================== RX END=============================================
}

void sw(const char toWrite[]){
	len = strlen(toWrite); //take size of characters to be printed
	k=0;	//initialize i
	UDR0=0; //make sure UDR0 is 0
	
	while (k<len) //while i is less than the total length of the characters to be printed
	{
		if (  ((UCSR0A & 0B00100000) == 0B00100000)  ){ //if UDRE0 is 1 (aka UDR0 is ready to send)
			UDR0 = toWrite[k]; //put the next character to be sent (now, UDRE0 is 0)
			k++;		//increase the position, and wait until UDRE0 is 1 again
		}
	}
	udr0 = '\0';
}

void swn(int num, int type, int ln) //take an int and print it on serial
{
	char str_intx[50];//declare a string of 50 characters
	
	itoa(num, str_intx, type);//convert from int to char and save it on str
	
	sw(str_intx); //serial write str
	
	if(ln == 1) //if we want a new line,
	{
		sw("\n\r");
	}
	
}

/*
Input: float number, output: print float number to serial, OR only return the float as string.
*/
void swf(double numf, uint8_t lnf, uint8_t extra_accur, uint8_t print_it) //Take a float and print it on serial
{

	if(extra_accur == 1)
	{
		sprintf(str_floatx,"%d.%02u", (int) numf, (int) fabs(((numf - (int) numf ) * 1000)));
	}
	else
	{
		sprintf(str_floatx,"%d.%02u", (int) numf, (int) fabs(((numf - (int) numf ) * 100))); //edited to accept negative numbers
	}

	//sprintf(str_floatx,"%d.%02u", (int) numf, (int) ((numf - (int) numf ) * 100) ); //this was te original function

	if(print_it)
	{
		sw(str_floatx);	
		
		if(lnf == 1) //if we want a new line,
		{
			sw("\n\r");
		}
	}
}

//==================================TX END=====================================

/*
the result (input) is stored in: rxBuffer
*/
void readUntill(char c_rx)
{
	rxWritePos = 0;//begin writing in the buffer from pos 0
	readString = 1;//interrupt will read and save strings
	rx_stop = c_rx; //interrupt will use the global var rx_stop to stop reading the string
	
	do{
		
	}while(readString == 1);
}
ISR(USART_RX_vect)
{
	if(readString == 1)
	{	
		rxBuffer[rxWritePos] = UDR0;
		if(rxBuffer[rxWritePos] == rx_stop)
		{
			readString = 0;
			/*when you initialize a character array by listing all of its characters 
			separately then you must supply the '\0' character explicitly */
			rxBuffer[rxWritePos] = '\0' ;
		}
		rxWritePos++;
	}
	else
	{
		udr0 = UDR0;	//udr0 variable can be used to store the input from the user.
	}
}

//==================================RX END==================================
